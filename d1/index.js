db.fruits.insertMany([
{
name : "Apple",
color : "Red",
stock : 20,
price: 40,
supplier_id : 1,
onSale : true,
origin: [ "Philippines", "US" ]
},
{
name : "Banana",
color : "Yellow",
stock : 15,
price: 20,
supplier_id : 2,
onSale : true,
origin: [ "Philippines", "Ecuador" ]
},
{
name : "Kiwi",
color : "Green",
stock : 25,
price: 50,
supplier_id : 1,
onSale : true,
origin: [ "US", "China" ]
},
{
name : "Mango",
color : "Yellow",
stock : 10,
price: 120,
supplier_id : 2,
onSale : false,
origin: [ "Philippines", "India" ]
}
]);

// MongoDB Aggregation

/*
	-Used to generate manipulated data and perform operations to create filtered results that helps analyzing data
	-compared to doing CRUD operations on our data from our previous sessions, aggregation gives us access to manipulate, filter, and compute for results, providing us with information to make necessary development decissions.
	-Aggregations in mongoDB are very flexible and you can form your own aggregation pipeline depending on the need of your application.

*/

// Aggregate methods


// match "$match"

/*
	-is used to pass the documents that meet the specified condition(s) to the nest pipeline stage/aggregation process
	Syntax:
		{$match: {field: value}}

*/

db.fruits.aggregate([
	{$match: {onSale: true}}
	]);

db.fruits.aggregate([
		{$match: {stock: {$gte: 20 }}}
	]);

// group "$group"
/*
	-used to group elements together field-value pairs using the data from the grouped element.
	Syntax:
		{$group: {_id: "value", fieldResult: "valueResult"}}
*/

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);


// sort "$sort"
/*
	-used when changing the order of aggregated results
	-providing a value of -1 will sort the documents in a descending order
	-providing a value of 1 will sort the documents in a ascending order
	Syntax:
		{$sort {field: 1/-1}}
*/

db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
	{ $sort: {total: 1} }
]);

// project "$project"

/*
	-used when aggregating data to include or exclude fields from the returned results
	Syntax:
		{$project: {field: 1/0}}
*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
	]);


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}},
		{$sort: {total: -1}}
	]);

// Aggregation pipeline

/*
	-aggregation pipeline in mongoDB is a framework for data aggregation
	-each stage transforms the documents as they pass through the deadlines
	Syntax:
		db.collectionName.aggregate({
			{stageA},
			{stageB},
			{stageC}
		});
*/

// operators

// sum "$sum"
/*
	-get the total of everything
*/
db.fruits.aggregate([
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);

// maximum "$max"

/*
	-gets the highest value out of everything else
*/

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
	]);


// minimum "$min"

/*
	-gets the lowest value out of everything else
*/

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
	]);


// average "$avg"

/*
	-gets the average value of all the fields
*/

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
	]);
